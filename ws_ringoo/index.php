<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.ws_ringoo
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;
$template 		 = 'templates/' . $this->template;




// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

if($task == "edit" || $layout == "form" ){
	$fullWidth = 1;
}else{
	$fullWidth = 0;
}

// get html head data
$head = $doc->getHeadData();
// remove deprecated meta-data (html5)
unset($head['metaTags']['http-equiv']);
unset($head['metaTags']['standard']['title']);
unset($head['metaTags']['standard']['rights']);
unset($head['metaTags']['standard']['language']);
// Robots if you wish
//unset($head['metaTags']['standard']['robots']);
if($user->guest){
// Mootools remove for unregistered
unset($head['scripts'][$this->baseurl . '/media/system/js/mootools-core.js']);
unset($head['scripts'][$this->baseurl . '/media/system/js/mootools-more.js']);
unset($head['scripts'][$this->baseurl . '/media/system/js/core.js']);
unset($head['scripts'][$this->baseurl . '/media/system/js/caption.js']);
unset($head['scripts'][$this->baseurl . '/media/system/js/modal.js']);
// unset($head['scripts'][$this->baseurl . '/media/jui/js/jquery-migrate.min.js']);
}
$doc->setHeadData($head); 



// Add JavaScript Frameworks
// JHtml::_('bootstrap.framework');
// $doc->addScript($template . '/js/template.js');
JHtml::_('jquery.framework', false);
$url = "//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js";
// $doc->addScript($url, $type="text/javascript");
$doc->addScript($template . '/js/custom7.js');
$doc->addScriptDeclaration("
jQuery(document).ready(function($){
  $('.socialtips, .sample').tipsy({fade: true, gravity: 's'});
	$('.ws-social, .opened').tipsy({fade: true, gravity: 'e' });
	$('.tipsB').tipsy({fade: true, gravity: 'n'});
	$('.morkva').tipsy({fade: true, gravity: 'se'});
	$('.fee').tipsy({fade: true, gravity: 'w'});
  $('div.hideWrap a.hideBtn').click(function(){
    $(this).toggleClass('show').siblings('div.hideCont').slideToggle('normal');
    return false;
  });
});
jQuery(function($){
	$('a.zoombox').zoombox({theme:'zoombox', opacity:0.8, duration:800, animation:true, autoplay:false});
});
jQuery(function(){
	$('#tabs, #itabs, #maptabs').tabs({ fxFade: true, fxSpeed: '200' });   
});
");

// Add Stylesheets
$stylelink = '<!--[if lte IE 7]><link rel="stylesheet" href="'. $template .'/css/ie7.css" type="text/css" /><![endif]-->' ."\n";
$stylelink .= '<!--[if IE 8]><link rel="stylesheet" href="'. $template .'/css/ie8.css" type="text/css" /><![endif]-->' ."\n";
$stylelink .= '<!--[if lt IE 9]><script src="'. $this->baseurl .'/media/jui/js/html5.js"></script><![endif]-->' ."\n";
$doc->addStyleSheet($template . '/css/style.v'. $this->params->get('ws_css_version') .'.css');
$doc->addCustomTag($stylelink);
// $doc->addStyleSheet($template . '/css/template.css');


$doc->setMetadata('viewport', 'width=device-width, initial-scale=1.0');
$doc->setMetadata('X-UA-Compatible', 'IE=edge,chrome=1', true);
$doc->setMetadata('SKYPE_TOOLBAR', 'SKYPE_TOOLBAR_PARSER_COMPATIBLE'); 
$doc->setGenerator('Create by webstyle.pp.ua');

// Load optional RTL Bootstrap CSS
// JHtml::_('bootstrap.loadCss', false, $this->direction);

// Adjusting content width
if ($this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span6";
}
elseif ($this->countModules('position-7') && !$this->countModules('position-8'))
{
	$span = "span9";
}
elseif (!$this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span9";
}
else
{
	$span = "span12";
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<jdoc:include type="head" />
	<?php // Use of Google Font ?>
	<?php if ($this->params->get('googleFont')) : ?>
		<link href='//fonts.googleapis.com/css?family=<?php echo $this->params->get('googleFontName'); ?>' rel='stylesheet' type='text/css' />
		<style type="text/css">
			h1,h2,h3,h4,h5,h6,.site-title{
				font-family: '<?php echo str_replace('+', ' ', $this->params->get('googleFontName')); ?>', sans-serif;
			}
		</style>
	<?php endif; ?>
</head>

<body class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '');
?>">
	<div id="ws-headerwrap">
    	<div id="ws-header">
        	<div id="ws-logo"><a href="<?php if ($this->language=="ru-ru") { ?><?php echo $this->baseurl; ?><?php } else { ?><?php echo $this->baseurl; ?>ua.html<?php } ?>" title="<?php //echo ; ?>"></a></div>
            <div id="ws-tool">
				<div id="ws-contact">
					<jdoc:include type="modules" name="user4" />
                </div>
            </div> 
			<jdoc:include type="modules" name="ws-lang" />
		</div>
        	<div id="ws-mainmenu">
            	<jdoc:include type="modules" name="ws-menu" />
            </div>
    </div> 
	<?php if($this->countModules('ws_gmap')) : ?>
		<jdoc:include type="modules" name="ws_gmap" style="raw" />
	<?php endif; ?>

	<?php if($this->countModules('ws-slider')) : ?>
<!--слайдер-->
		<jdoc:include type="modules" name="ws-slider" style="raw" />
<!--/слайдер-->		
	<?php endif; ?>



<?php if($this->countModules('ws-player2')) : ?>
<div class="ws-player2">
	<jdoc:include type="modules" name="ws-player2" style="wsgkhome" />
 	<jdoc:include type="modules" name="ws-news-gk2" style="wsgkhome" />
</div>
<?php endif; ?>


<?php if($this->countModules('ws-player')) : ?>
<div class="ws-player">
	<jdoc:include type="modules" name="ws-player" style="raw" />
 	<jdoc:include type="modules" name="ws-news-gk" style="raw" />
</div>
<?php endif; ?>

    	<div id="ws-container">
			<jdoc:include type="modules" name="ws-kvytky" style="wskv" />
			<?php if($this->countModules('wsshow')) : ?>
				<div id="ws-show">
					<jdoc:include type="modules" name="wsshow" style="xhtml" />
				</div>
			<?php endif; ?>
            
			<?php if($this->countModules('slideshow')) : ?>
			<!-- начало слайдшоу -->
				<div id="ws-slideshow">
					<jdoc:include type="modules" name="slideshow" style="xhtml" />
				</div>
			<!-- конец слайдшоу -->	
			<?php endif; ?>
            
            	 <?php if($this->countModules('featured')) : ?>
                 <div id="ws-featured">
						<jdoc:include type="modules" name="featured" style="xhtml" />
                 </div>
                 <?php endif; ?>
                 <jdoc:include type="message" />
                 <jdoc:include type="component" />
            
            
			<?php if($this->countModules('right')) : ?>
            <div id="ws-colwrap">
                <jdoc:include type="modules" name="right" style="rounded" />
			</div>
			<?php endif; ?>



            <?php if($this->countModules('user6') || $this->countModules('user7')) : ?>
            <!-- модули user6 и user7 -->
            <div id="ws-userbox">
            	<?php if ($this->countModules('user6')) : ?>
					<div id="ws-box6<?php echo $div_border; ?>" class="ws-userbox" style="width: <?php echo $div_userbox; ?>">
						<jdoc:include type="modules" name="user6" style="xhtml" />
					</div>
				<?php endif; ?>
				<?php if ($this->countModules('user7')) : ?>
					<div id="ws-box7" class="ws-userbox" style="width: <?php echo $div_userbox; ?>">
						<jdoc:include type="modules" name="user7" style="xhtml" />
					</div>
				<?php endif; ?>
            </div>
            <?php endif; ?>
		</div>
	<?php if ($this->countModules('user5')) : ?>
		<jdoc:include type="modules" name="user5" style="raw" />
	<?php endif; ?>

    <div id="ws-footerwrap">
    	<div id="ws-footer">
				<div class="ws-footer-menu"><jdoc:include type="modules" name="botmenu" style="wsheadspan" /></div>
				<div class="ws-footer-help"><jdoc:include type="modules" name="helpfull" style="wsheadspan" /></div>
				<div class="ws-footer-menu"><jdoc:include type="modules" name="botmenu2" style="wsheadspan" /></div>
				<div class="ws-socials"><jdoc:include type="modules" name="socials" /></div>
				<div class="ws-copyleft"><jdoc:include type="modules" name="copyright" /></div>
    	</div>
    </div>
<jdoc:include type="modules" name="debug" />
<?php if ($this->countModules('left')) : ?>
	<jdoc:include type="modules" name="left" style="raw" />
<?php endif; ?>
<?php //require(dirname(__FILE__).DS.'/metrika.php');?>
</body>
</html>