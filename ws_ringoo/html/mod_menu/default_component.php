<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.
$class = $item->anchor_css ? 'class="' . $item->anchor_css . '" ' : '';
$title = $item->anchor_title ? 'title="' . $item->anchor_title . '" ' : '';

if ($item->menu_image)
{
	$item->params->get('menu_text', 1) ?
	$linktype = '<img src="' . $item->menu_image . '" align="left" alt="' . $item->title . '" /><span class="wsp_title">' . $item->title . '</span> ' :
	$linktype = '<img src="' . $item->menu_image . '" alt="' . $item->title . '" />';
}
else
{
	$linktype = $item->title;
}

$new_link = explode("||", $linktype);

if (isset($new_link[1])) {
	$new_link = $new_link[0] . '<br /><span class="vt_desc">' . $new_link[1] . '</span>';
} else {
	$new_link = $new_link[0];
}

switch ($item->browserNav) {
	default:
	case 0:
	$aliasToId = $item->params->get('aliasoptions');
		// if (in_array($item->id, $path) || in_array($aliasToId , $path)){
		if ($item->id == $active_id) {
			echo '<span class="nolink">'.$new_link.'</span>';
		}else {
			?><a <?php echo $class; ?>href="<?php echo $item->flink; ?>" <?php echo $title; ?>><?php echo $new_link ?></a><?php
		}
		break;
	case 1:
		// _blank
?><a <?php echo $class; ?>href="<?php echo $item->flink; ?>" target="_blank" <?php echo $title; ?>><?php echo $new_link ?></a><?php
		break;
	case 2:
	// Use JavaScript "window.open"
?><a <?php echo $class; ?>href="<?php echo $item->flink; ?>" onclick="window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes');return false;" <?php echo $title; ?>><?php echo $new_link ?></a>
<?php
		break;
}
