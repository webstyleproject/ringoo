<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

/**
 * Toogle HTML class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_ws_ringoo
 * @since       2.5
 */
abstract class JHtmlBank
{


	/**
	 * Returns a bank status state on a grid
	 *
	 * @param   integer  $value     The state value.
	 * @param   integer  $i         The row index
	 * @param   boolean  $enabled   An optional setting for access control on the action.
	 * @param   string   $checkbox  An optional prefix for checkboxes.
	 *
	 * @return  string   The Html code
	 *
	 * @see     JHtmlJGrid::state
	 *
	 * @since   2.5.5
	 */
	public static function redesign($value, $i, $enabled = true, $checkbox = 'cb')
	{
		$states = array(
			1 => array(
				'redesign_unpublish',
				'COM_WS_RINGOO_PUBLISHED',
				'COM_WS_RINGOO_HTML_PUBLISH_BANK',
				'COM_WS_RINGOO_PUBLISHED',
				true,
				'publish',
				'publish'
			),
			0 => array(
				'redesign_publish',
				'COM_WS_RINGOO_UNPUBLISHED',
				'COM_WS_RINGOO_HTML_UNPUBLISH_BANK',
				'COM_WS_RINGOO_UNPUBLISHED',
				true,
				'unpublish',
				'unpublish'
			),
		);

		return JHtml::_('jgrid.state', $states, $value, $i, 'ringoocitys.', $enabled, true, $checkbox);
	}

	public static function vlasniyrahunok($value, $i, $enabled = true, $checkbox = 'cb')
	{
		$states = array(
			1 => array(
				'vlasniyrahunok_unpublish',
				'COM_WS_RINGOO_PUBLISHED',
				'COM_WS_RINGOO_HTML_PUBLISH_BANK',
				'COM_WS_RINGOO_PUBLISHED',
				true,
				'publish',
				'publish'
			),
			0 => array(
				'vlasniyrahunok_publish',
				'COM_WS_RINGOO_UNPUBLISHED',
				'COM_WS_RINGOO_HTML_UNPUBLISH_BANK',
				'COM_WS_RINGOO_UNPUBLISHED',
				true,
				'unpublish',
				'unpublish'
			),
		);

		return JHtml::_('jgrid.state', $states, $value, $i, 'ringoocitys.', $enabled, true, $checkbox);
	}

	public static function tickets($value, $i, $enabled = true, $checkbox = 'cb')
	{
		$states = array(
			1 => array(
				'tickets_unpublish',
				'COM_WS_RINGOO_PUBLISHED',
				'COM_WS_RINGOO_HTML_PUBLISH_BANK',
				'COM_WS_RINGOO_PUBLISHED',
				true,
				'publish',
				'publish'
			),
			0 => array(
				'tickets_publish',
				'COM_WS_RINGOO_UNPUBLISHED',
				'COM_WS_RINGOO_HTML_UNPUBLISH_BANK',
				'COM_WS_RINGOO_UNPUBLISHED',
				true,
				'unpublish',
				'unpublish'
			),
		);

		return JHtml::_('jgrid.state', $states, $value, $i, 'ringoocitys.', $enabled, true, $checkbox);
	}

	public static function privat($value, $i, $enabled = true, $checkbox = 'cb')
	{
		$states = array(
			1 => array(
				'privatbank_unpublish',
				'COM_WS_RINGOO_PUBLISHED',
				'COM_WS_RINGOO_HTML_PUBLISH_BANK',
				'COM_WS_RINGOO_PUBLISHED',
				true,
				'publish',
				'publish'
			),
			0 => array(
				'privatbank_publish',
				'COM_WS_RINGOO_UNPUBLISHED',
				'COM_WS_RINGOO_HTML_UNPUBLISH_BANK',
				'COM_WS_RINGOO_UNPUBLISHED',
				true,
				'unpublish',
				'unpublish'
			),
		);

		return JHtml::_('jgrid.state', $states, $value, $i, 'ringoocitys.', $enabled, true, $checkbox);
	}
	public static function platinum($value, $i, $enabled = true, $checkbox = 'cb')
	{
		$states = array(
			1 => array(
				'platinumbank_unpublish',
				'COM_WS_RINGOO_PUBLISHED',
				'COM_WS_RINGOO_HTML_PUBLISH_BANK',
				'COM_WS_RINGOO_PUBLISHED',
				true,
				'publish',
				'publish'
			),
			0 => array(
				'platinumbank_publish',
				'COM_WS_RINGOO_UNPUBLISHED',
				'COM_WS_RINGOO_HTML_UNPUBLISH_BANK',
				'COM_WS_RINGOO_UNPUBLISHED',
				true,
				'unpublish',
				'unpublish'
			),
		);

		return JHtml::_('jgrid.state', $states, $value, $i, 'ringoocitys.', $enabled, true, $checkbox);
	}
	public static function delta($value, $i, $enabled = true, $checkbox = 'cb')
	{
		$states = array(
			1 => array(
				'deltabank_unpublish',
				'COM_WS_RINGOO_PUBLISHED',
				'COM_WS_RINGOO_HTML_PUBLISH_BANK',
				'COM_WS_RINGOO_PUBLISHED',
				true,
				'publish',
				'publish'
			),
			0 => array(
				'deltabank_publish',
				'COM_WS_RINGOO_UNPUBLISHED',
				'COM_WS_RINGOO_HTML_UNPUBLISH_BANK',
				'COM_WS_RINGOO_UNPUBLISHED',
				true,
				'unpublish',
				'unpublish'
			),
		);

		return JHtml::_('jgrid.state', $states, $value, $i, 'ringoocitys.', $enabled, true, $checkbox);
	}
	public static function alfa($value, $i, $enabled = true, $checkbox = 'cb')
	{
		$states = array(
			1 => array(
				'alfabank_unpublish',
				'COM_WS_RINGOO_PUBLISHED',
				'COM_WS_RINGOO_HTML_PUBLISH_BANK',
				'COM_WS_RINGOO_PUBLISHED',
				true,
				'publish',
				'publish'
			),
			0 => array(
				'alfabank_publish',
				'COM_WS_RINGOO_UNPUBLISHED',
				'COM_WS_RINGOO_HTML_UNPUBLISH_BANK',
				'COM_WS_RINGOO_UNPUBLISHED',
				true,
				'unpublish',
				'unpublish'
			),
		);

		return JHtml::_('jgrid.state', $states, $value, $i, 'ringoocitys.', $enabled, true, $checkbox);
	}
	public static function otp($value, $i, $enabled = true, $checkbox = 'cb')
	{
		$states = array(
			1 => array(
				'otpbank_unpublish',
				'COM_WS_RINGOO_PUBLISHED',
				'COM_WS_RINGOO_HTML_PUBLISH_BANK',
				'COM_WS_RINGOO_PUBLISHED',
				true,
				'publish',
				'publish'
			),
			0 => array(
				'otpbank_publish',
				'COM_WS_RINGOO_UNPUBLISHED',
				'COM_WS_RINGOO_HTML_UNPUBLISH_BANK',
				'COM_WS_RINGOO_UNPUBLISHED',
				true,
				'unpublish',
				'unpublish'
			),
		);

		return JHtml::_('jgrid.state', $states, $value, $i, 'ringoocitys.', $enabled, true, $checkbox);
	}



}
