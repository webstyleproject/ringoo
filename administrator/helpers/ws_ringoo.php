<?php

/**
 * @version     1.0.0
 * @package     com_ws_ringoo
 * @copyright   Copyright WebStyle project (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Svyat <admin@webstyle.pp.ua> - http://webstyle.pp.ua
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Ws_ringoo helper.
 */
class Ws_ringooHelper {

    /**
     * Configure the Linkbar.
     */
    public static function addSubmenu($vName = '') {
		JHtmlSidebar::addEntry(
			JText::_('COM_WS_RINGOO_TITLE_RINGOOREGIONS'),
			'index.php?option=com_ws_ringoo&view=ringooregions',
			$vName == 'ringooregions'
		);
        JHtmlSidebar::addEntry(
            JText::_('COM_WS_RINGOO_TITLE_RINGOOCITYS'),
            'index.php?option=com_ws_ringoo&view=ringoocitys',
            $vName == 'ringoocitys'
        );
        JHtmlSidebar::addEntry(
            JText::_('COM_WS_RINGOO_TITLE_RINGOOSUPERPRICES'),
            'index.php?option=com_ws_ringoo&view=ringoosuperprices',
            $vName == 'ringoosuperprices'
        );
        JHtmlSidebar::addEntry(
            JText::_('COM_WS_RINGOO_TITLE_RINGOOBANNERS'),
            'index.php?option=com_ws_ringoo&view=ringoobanners',
            $vName == 'ringoobanners'
        );

    }

    /**
     * Gets a list of the actions that can be performed.
     *
     * @return	JObject
     * @since	1.6
     */
    public static function getActions() {
        $user = JFactory::getUser();
        $result = new JObject;

        $assetName = 'com_ws_ringoo';

        $actions = array(
            'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
        );

        foreach ($actions as $action) {
            $result->set($action, $user->authorise($action, $assetName));
        }

        return $result;
    }
    
    /**
    * Get group name using group ID
    * @param integer $menuitem_id Menuitem ID
    * @return mixed group name if the group was found, null otherwise
    */
    public static function getMenuItemNameByMenuItemId($menuitem_id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query
            ->select('title')
            ->from('#__menu')
            ->where('id = ' . intval($menuitem_id));

        $db->setQuery($query);
        return $db->loadResult();
    }


}
