<?php

/**
 * @version     1.0.0
 * @package     com_ws_ringoo
 * @copyright   Copyright WebStyle project (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Svyat <admin@webstyle.pp.ua> - http://webstyle.pp.ua
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Ws_ringoo helper for publish function.
 */

abstract class JHtmlItem
{
   /**
    * @param   int $value   The featured value
    * @param   int $i
    * @param   bool $canChange Whether the value can be changed or not
    *
    * @return   string   The anchor tag to toggle featured/unfeatured contacts.
    * @since   1.6
    */
   static function featured($value = 0, $i, $canChange = true)
   {
      $modelname = jRequest::getVar('view');
      // Array of image, task, title, action
      $states   = array(
         0   => array('disabled.png', $modelname.'.featured', 'COM_TOURMANAGER_UNFEATURED', 'COM_TOURMANAGER_TOGGLE_TO_FEATURE'),
         1   => array('featured.png', $modelname.'.unfeatured', 'JFEATURED', 'COM_TOURMANAGER_TOGGLE_TO_UNFEATURE'),
      );
      $state   = JArrayHelper::getValue($states, (int) $value, $states[1]);
      $html   = JHtml::_('image','admin/'.$state[0], JText::_($state[2]), NULL, true);
      if ($canChange) {
         $html   = '<a href="#" onclick="return listItemTask(\'cb'.$i.'\',\''.$state[1].'\')" title="'.JText::_($state[3]).'">'
               . $html .'</a>';
      }

      return $html;
   }
}
