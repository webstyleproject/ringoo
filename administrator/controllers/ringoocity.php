<?php
/**
 * @version     1.0.0
 * @package     com_ws_ringoo
 * @copyright   Copyright WebStyle project (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Svyat <admin@webstyle.pp.ua> - http://webstyle.pp.ua
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Ringoocity controller class.
 */
class Ws_ringooControllerRingoocity extends JControllerForm
{

    function __construct() {
        $this->view_list = 'ringoocitys';
        parent::__construct();
    }

}