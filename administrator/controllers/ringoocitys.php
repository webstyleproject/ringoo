<?php
/**
 * @version     1.0.0
 * @package     com_ws_ringoo
 * @copyright   Copyright WebStyle project (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Svyat <admin@webstyle.pp.ua> - http://webstyle.pp.ua
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

/**
 * Ringoocitys list controller class.
 */
class Ws_ringooControllerRingoocitys extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		$this->registerTask('redesign_unpublish', 'redesign_publish');
		$this->registerTask('tickets_unpublish', 'tickets_publish');
		$this->registerTask('vlasniyrahunok_unpublish', 'vlasniyrahunok_publish');

		$this->registerTask('privatbank_unpublish', 'privatbank_publish');
		$this->registerTask('platinumbank_unpublish', 'platinumbank_publish');
		$this->registerTask('deltabank_unpublish', 'deltabank_publish');
		$this->registerTask('alfabank_unpublish', 'alfabank_publish');
		$this->registerTask('otpbank_unpublish', 'otpbank_publish');

	}


	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function getModel($name = 'ringoocity', $prefix = 'Ws_ringooModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}


	/**
	 * PrivatBank items
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function privatbank_publish()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$ids    = $this->input->get('cid', array(), 'array');
		$values = array('privatbank_publish' => 1, 'privatbank_unpublish' => 0);
		$task   = $this->getTask();
		$value  = JArrayHelper::getValue($values, $task, 0, 'int');

		if (empty($ids))
		{
			JError::raiseWarning(500, JText::_('Ошибка мать его'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			// Change the state of the records.
			if (!$model->privatbank($ids, $value))
			{
				JError::raiseWarning(500, $model->getError());
			}
			else
			{
				if ($value == 1)
				{
					$ntext = 'ПриватБанк подключен к магазину';
				}
				else
				{
					$ntext = 'ПриватБанк отключен';
				}

				$this->setMessage(JText::plural($ntext, count($ids)));
			}
		}

		$this->setRedirect('index.php?option=com_ws_ringoo&view=ringoocitys');
	}    

	/**
	 * PlatinumBank items
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function platinumbank_publish()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$ids    = $this->input->get('cid', array(), 'array');
		$values = array('platinumbank_publish' => 1, 'platinumbank_unpublish' => 0);
		$task   = $this->getTask();
		$value  = JArrayHelper::getValue($values, $task, 0, 'int');

		if (empty($ids))
		{
			JError::raiseWarning(500, JText::_('Ошибка мать его'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			// Change the state of the records.
			if (!$model->platinum($ids, $value))
			{
				JError::raiseWarning(500, $model->getError());
			}
			else
			{
				if ($value == 1)
				{
					$ntext = 'Платинум Банк подключен к магазину';
				}
				else
				{
					$ntext = 'Платинум Банк отключен';
				}

				$this->setMessage(JText::plural($ntext, count($ids)));
			}
		}

		$this->setRedirect('index.php?option=com_ws_ringoo&view=ringoocitys');
	}
	/**
	 * AlfaBank items
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function alfabank_publish()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$ids    = $this->input->get('cid', array(), 'array');
		$values = array('alfabank_publish' => 1, 'alfabank_unpublish' => 0);
		$task   = $this->getTask();
		$value  = JArrayHelper::getValue($values, $task, 0, 'int');

		if (empty($ids))
		{
			JError::raiseWarning(500, JText::_('Ошибка мать его'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			// Change the state of the records.
			if (!$model->alfa($ids, $value))
			{
				JError::raiseWarning(500, $model->getError());
			}
			else
			{
				if ($value == 1)
				{
					$ntext = 'Альфа Банк подключен к магазину';
				}
				else
				{
					$ntext = 'Альфа Банк отключен';
				}

				$this->setMessage(JText::plural($ntext, count($ids)));
			}
		}

		$this->setRedirect('index.php?option=com_ws_ringoo&view=ringoocitys');
	}

	/**
	 * DeltaBank items
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function deltabank_publish()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$ids    = $this->input->get('cid', array(), 'array');
		$values = array('deltabank_publish' => 1, 'deltabank_unpublish' => 0);
		$task   = $this->getTask();
		$value  = JArrayHelper::getValue($values, $task, 0, 'int');

		if (empty($ids))
		{
			JError::raiseWarning(500, JText::_('Ошибка мать его'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			// Change the state of the records.
			if (!$model->delta($ids, $value))
			{
				JError::raiseWarning(500, $model->getError());
			}
			else
			{
				if ($value == 1)
				{
					$ntext = 'Дельта Банк подключен к магазину';
				}
				else
				{
					$ntext = 'Дельта Банк отключен';
				}

				$this->setMessage(JText::plural($ntext, count($ids)));
			}
		}

		$this->setRedirect('index.php?option=com_ws_ringoo&view=ringoocitys');
	}

	/**
	 * OTPBank items
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function otpbank_publish()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$ids    = $this->input->get('cid', array(), 'array');
		$values = array('otpbank_publish' => 1, 'otpbank_unpublish' => 0);
		$task   = $this->getTask();
		$value  = JArrayHelper::getValue($values, $task, 0, 'int');

		if (empty($ids))
		{
			JError::raiseWarning(500, JText::_('Ошибка мать его'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			// Change the state of the records.
			if (!$model->otp($ids, $value))
			{
				JError::raiseWarning(500, $model->getError());
			}
			else
			{
				if ($value == 1)
				{
					$ntext = 'ОТП Банк подключен к магазину';
				}
				else
				{
					$ntext = 'ОТП Банк отключен';
				}

				$this->setMessage(JText::plural($ntext, count($ids)));
			}
		}

		$this->setRedirect('index.php?option=com_ws_ringoo&view=ringoocitys');
	}

	/**
	 * Redesign items
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function redesign_publish()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$ids    = $this->input->get('cid', array(), 'array');
		$values = array('redesign_publish' => 1, 'redesign_unpublish' => 0);
		$task   = $this->getTask();
		$value  = JArrayHelper::getValue($values, $task, 0, 'int');

		if (empty($ids))
		{
			JError::raiseWarning(500, JText::_('Ошибка мать его'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			// Change the state of the records.
			if (!$model->wsredesign($ids, $value))
			{
				JError::raiseWarning(500, $model->getError());
			}
			else
			{
				if ($value == 1)
				{
					$ntext = 'Магазин закрыт на ремонт-редизайн ';
				}
				else
				{
					$ntext = 'Магазин открыт. Ремонт-редизайн окончен';
				}

				$this->setMessage(JText::plural($ntext, count($ids)));
			}
		}

		$this->setRedirect('index.php?option=com_ws_ringoo&view=ringoocitys');
	}

	/**
	 * Tickets items
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function tickets_publish()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$ids    = $this->input->get('cid', array(), 'array');
		$values = array('tickets_publish' => 1, 'tickets_unpublish' => 0);
		$task   = $this->getTask();
		$value  = JArrayHelper::getValue($values, $task, 0, 'int');

		if (empty($ids))
		{
			JError::raiseWarning(500, JText::_('Ошибка мать его'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			// Change the state of the records.
			if (!$model->tickets($ids, $value))
			{
				JError::raiseWarning(500, $model->getError());
			}
			else
			{
				if ($value == 1)
				{
					$ntext = 'Билеты подключены к магазину';
				}
				else
				{
					$ntext = 'Билеты отключены';
				}

				$this->setMessage(JText::plural($ntext, count($ids)));
			}
		}

		$this->setRedirect('index.php?option=com_ws_ringoo&view=ringoocitys');
	}
	/**
	 * Vlasniyrahunok items
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function vlasniyrahunok_publish()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$ids    = $this->input->get('cid', array(), 'array');
		$values = array('vlasniyrahunok_publish' => 1, 'vlasniyrahunok_unpublish' => 0);
		$task   = $this->getTask();
		$value  = JArrayHelper::getValue($values, $task, 0, 'int');

		if (empty($ids))
		{
			JError::raiseWarning(500, JText::_('Ошибка мать его'));
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			// Change the state of the records.
			if (!$model->vlasniyrahunok($ids, $value))
			{
				JError::raiseWarning(500, $model->getError());
			}
			else
			{
				if ($value == 1)
				{
					$ntext = 'Власний Рахунок подключен к магазину';
				}
				else
				{
					$ntext = 'Власний Рахунок отключен';
				}

				$this->setMessage(JText::plural($ntext, count($ids)));
			}
		}

		$this->setRedirect('index.php?option=com_ws_ringoo&view=ringoocitys');
	}


	/**
	 * Method to save the submitted ordering values for records via AJAX.
	 *
	 * @return  void
	 *
	 * @since   3.0
	 */
	public function saveOrderAjax()
	{
		// Get the input
		$input = JFactory::getApplication()->input;
		$pks = $input->post->get('cid', array(), 'array');
		$order = $input->post->get('order', array(), 'array');

		// Sanitize the input
		JArrayHelper::toInteger($pks);
		JArrayHelper::toInteger($order);

		// Get the model
		$model = $this->getModel();

		// Save the ordering
		$return = $model->saveorder($pks, $order);

		if ($return)
		{
			echo "1";
		}

		// Close the application
		JFactory::getApplication()->close();
	}
    
    
    
}