<?php

/**
 * @version     1.0.0
 * @package     com_ws_ringoo
 * @copyright   Copyright WebStyle project (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Svyat <admin@webstyle.pp.ua> - http://webstyle.pp.ua
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Ws_ringoo.
 */
class Ws_ringooViewRingoocitys extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;

    /**
     * Display the view
     */
    public function display($tpl = null) {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
        }

        Ws_ringooHelper::addSubmenu('ringoocitys');

        $this->addToolbar();

        $this->sidebar = JHtmlSidebar::render();
        parent::display($tpl);
    }

    /**
     * Add the page title and toolbar.
     *
     * @since	1.6
     */
    protected function addToolbar() {
        require_once JPATH_COMPONENT . '/helpers/ws_ringoo.php';

        $state = $this->get('State');
        $canDo = Ws_ringooHelper::getActions($state->get('filter.category_id'));

        JToolBarHelper::title(JText::_('COM_WS_RINGOO_TITLE_RINGOOCITYS'), 'ringoocitys.png');

        //Check if the form exists before showing the add/edit buttons
        $formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/ringoocity';
        if (file_exists($formPath)) {

            if ($canDo->get('core.create')) {
                JToolBarHelper::addNew('ringoocity.add', 'JTOOLBAR_NEW');
            }

            if ($canDo->get('core.edit') && isset($this->items[0])) {
                JToolBarHelper::editList('ringoocity.edit', 'JTOOLBAR_EDIT');
            }
        }


        if ($canDo->get('core.edit.state')) {

            if (isset($this->items[0]->state)) {
                JToolBarHelper::divider();
                JToolBarHelper::custom('ringoocitys.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
                JToolBarHelper::custom('ringoocitys.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
            } else if (isset($this->items[0])) {
                //If this component does not use state then show a direct delete button as we can not trash
                JToolBarHelper::deleteList('', 'ringoocitys.delete', 'JTOOLBAR_DELETE');
            }

            if (isset($this->items[0]->state)) {
                JToolBarHelper::divider();
                JToolBarHelper::archiveList('ringoocitys.archive', 'JTOOLBAR_ARCHIVE');
            }
            if (isset($this->items[0]->checked_out)) {
                JToolBarHelper::custom('ringoocitys.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
            }
        }

        //Show trash and delete for components that uses the state field
        if (isset($this->items[0]->state)) {
            if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
                JToolBarHelper::deleteList('', 'ringoocitys.delete', 'JTOOLBAR_EMPTY_TRASH');
                JToolBarHelper::divider();
            } else if ($canDo->get('core.edit.state')) {
                JToolBarHelper::trash('ringoocitys.trash', 'JTOOLBAR_TRASH');
                JToolBarHelper::divider();
            }
        }

        if ($canDo->get('core.admin')) {
            JToolBarHelper::preferences('com_ws_ringoo');
        }

        //Set sidebar action - New in 3.0
        JHtmlSidebar::setAction('index.php?option=com_ws_ringoo&view=ringoocitys');

        JHtmlSidebar::addFilter(
            JText::_('JOPTION_SELECT_LANGUAGE'),
            'filter_language',
            JHtml::_('select.options', JHtml::_('contentlanguage.existing', true, true), 'value', 'text', $this->state->get('filter.language'))
        );
        
        $this->extra_sidebar = '';
        
		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',

			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

		);
        //Filter for the field ".region;
        jimport('joomla.form.form');
        $options = array();
        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
        $form = JForm::getInstance('com_ws_ringoo.ringoocity', 'ringoocity');

        $field = $form->getField('region');

        $query = $form->getFieldAttribute('filter_region','query');
        $translate = $form->getFieldAttribute('filter_region','translate');
        $key = $form->getFieldAttribute('filter_region','key_field');
        $value = $form->getFieldAttribute('filter_region','value_field');

        // Get the database object.
        $db = JFactory::getDBO();

        // Set the query and get the result list.
        $db->setQuery($query);
        $items = $db->loadObjectlist();

        // Build the field options.
        if (!empty($items))
        {
            foreach ($items as $item)
            {
                if ($translate == true)
                {
                    $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                }
                else
                {
                    $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                }
            }
        }

        JHtmlSidebar::addFilter(
            '$Область',
            'filter_region',
            JHtml::_('select.options', $options, "value", "text", $this->state->get('filter.region')),
            true
        );
    }

	protected function getSortFields()
	{
		return array(
		'a.id' => JText::_('JGRID_HEADING_ID'),
		'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
		'a.state' => JText::_('JSTATUS'),
		'a.name' => JText::_('COM_WS_RINGOO_RINGOOCITYS_NAME'),
		'a.region' => JText::_('COM_WS_RINGOO_RINGOOCITYS_REGION'),
		'a.address' => JText::_('COM_WS_RINGOO_RINGOOCITYS_ADDRESS'),
		'a.redesign' => JText::_('COM_WS_RINGOO_RINGOOCITYS_REDESIGN'),
		'a.vlasniyrahunok' => JText::_('COM_WS_RINGOO_RINGOOCITYS_VLASNIYRAHUNOK'),
		'a.tickets' => JText::_('COM_WS_RINGOO_RINGOOCITYS_TICKETS'),
		'a.alfabank' => JText::_('COM_WS_RINGOO_RINGOOCITYS_ALFABANK'),
		'a.platinumbank' => JText::_('COM_WS_RINGOO_RINGOOCITYS_PLATINUMBANK'),
		'a.otpbank' => JText::_('COM_WS_RINGOO_RINGOOCITYS_OTPBANK'),
		'a.privatbank' => JText::_('COM_WS_RINGOO_RINGOOCITYS_PRIVATBANK'),
		'a.deltabank' => JText::_('COM_WS_RINGOO_RINGOOCITYS_DELTABANK'),
        'a.language' => JText::_('JGRID_HEADING_LANGUAGE'),
		);
	}

}
