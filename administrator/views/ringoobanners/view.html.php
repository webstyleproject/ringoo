<?php

/**
 * @version     1.0.0
 * @package     com_ws_ringoo
 * @copyright   Copyright WebStyle project (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Svyat <admin@webstyle.pp.ua> - http://webstyle.pp.ua
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Ws_ringoo.
 */
class Ws_ringooViewRingoobanners extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;

    /**
     * Display the view
     */
    public function display($tpl = null) {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
        }

        Ws_ringooHelper::addSubmenu('ringoobanners');

        $this->addToolbar();

        $this->sidebar = JHtmlSidebar::render();
        parent::display($tpl);
    }

    /**
     * Add the page title and toolbar.
     *
     * @since	1.6
     */
    protected function addToolbar() {
        require_once JPATH_COMPONENT . '/helpers/ws_ringoo.php';

        $state = $this->get('State');
        $canDo = Ws_ringooHelper::getActions($state->get('filter.category_id'));

        JToolBarHelper::title(JText::_('COM_WS_RINGOO_TITLE_RINGOOBANNERS'), 'ringoobanners.png');

        //Check if the form exists before showing the add/edit buttons
        $formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/ringoobanner';
        if (file_exists($formPath)) {

            if ($canDo->get('core.create')) {
                JToolBarHelper::addNew('ringoobanner.add', 'JTOOLBAR_NEW');
            }

            if ($canDo->get('core.edit') && isset($this->items[0])) {
                JToolBarHelper::editList('ringoobanner.edit', 'JTOOLBAR_EDIT');
            }
        }

        if ($canDo->get('core.edit.state')) {

            if (isset($this->items[0]->state)) {
                JToolBarHelper::divider();
                JToolBarHelper::custom('ringoobanners.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
                JToolBarHelper::custom('ringoobanners.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
            } else if (isset($this->items[0])) {
                //If this component does not use state then show a direct delete button as we can not trash
                JToolBarHelper::deleteList('', 'ringoobanners.delete', 'JTOOLBAR_DELETE');
            }

            if (isset($this->items[0]->state)) {
                JToolBarHelper::divider();
                JToolBarHelper::archiveList('ringoobanners.archive', 'JTOOLBAR_ARCHIVE');
            }
            if (isset($this->items[0]->checked_out)) {
                JToolBarHelper::custom('ringoobanners.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
            }
        }

        //Show trash and delete for components that uses the state field
        if (isset($this->items[0]->state)) {
            if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
                JToolBarHelper::deleteList('', 'ringoobanners.delete', 'JTOOLBAR_EMPTY_TRASH');
                JToolBarHelper::divider();
            } else if ($canDo->get('core.edit.state')) {
                JToolBarHelper::trash('ringoobanners.trash', 'JTOOLBAR_TRASH');
                JToolBarHelper::divider();
            }
        }
        //Filter for the field category
        $select_label = JText::sprintf('COM_RINGOO_FILTER_SELECT_LABEL', 'Категория');
        $options = array();
        $options[0] = new stdClass();
        $options[0]->value = "home";
        $options[0]->text = "На главной";
        $options[1] = new stdClass();
        $options[1]->value = "page";
        $options[1]->text = "Внутренние";
        JHtmlSidebar::addFilter(
            $select_label,
            'filter_category',
            JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.category'), true)
        );

        if ($canDo->get('core.admin')) {
            JToolBarHelper::preferences('com_ws_ringoo');
        }

        //Set sidebar action - New in 3.0
        JHtmlSidebar::setAction('index.php?option=com_ws_ringoo&view=ringoobanners');

        $this->extra_sidebar = '';
        
		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',

			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

		);
        JHtmlSidebar::addFilter(
            JText::_('JOPTION_SELECT_LANGUAGE'),
            'filter_language',
            JHtml::_('select.options', JHtml::_('contentlanguage.existing', true, true), 'value', 'text', $this->state->get('filter.language'))
        );

    }

	protected function getSortFields()
	{
		return array(
		'a.image' => JText::_('COM_WS_RINGOO_RINGOOBANNERS_IMAGE'),
		'a.name' => JText::_('COM_WS_RINGOO_RINGOOBANNERS_NAME'),
		'a.state' => JText::_('JSTATUS'),
		'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
        'a.category' => JText::_('COM_WS_RINGOO_RINGOOBANNERS_CATEGORY'),
		'a.id' => JText::_('JGRID_HEADING_ID'),
        'a.language' => JText::_('JGRID_HEADING_LANGUAGE'),
		);
	}

}
