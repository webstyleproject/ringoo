<?php
/**
 * @version     1.0.0
 * @package     com_ws_ringoo
 * @copyright   Copyright WebStyle project (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Svyat <admin@webstyle.pp.ua> - http://webstyle.pp.ua
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_ws_ringoo/assets/css/ws_ringoo.css');
$document->addScript('//maps.google.com/maps/api/js?sensor=false');
// $document->addScript('//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
$document->addScript('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js');
$document->addScript( JURI::base().'components/com_ws_ringoo/assets/js/jquery.ui.addresspicker.js' );

$js_code = "
var j = jQuery.noConflict();

  j(function() {
    var addresspickerMap = j('#addresspicker_map').addresspicker({
      regionBias:'ua',
      mapOptions:{
        zoom: 6,
        center: new google.maps.LatLng(50, 30),
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      },
      elements:{
        map:'#map',
        lat:'#jform_longitude',
        lng:'#jform_latitude',
      }
    });

    var gmarker = addresspickerMap.addresspicker('marker');
    gmarker.setVisible(true);
    addresspickerMap.addresspicker('updatePosition');
  });
";
$document->addScriptDeclaration($js_code);



?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function() {
        
	js('input:hidden.region').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('regionhidden')){
			js('#jform_region option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_region").trigger("liszt:updated");
    });

    Joomla.submitbutton = function(task)
    {
        if (task == 'ringoocity.cancel') {
            Joomla.submitform(task, document.getElementById('ringoocity-form'));
        }
        else {
            
            if (task != 'ringoocity.cancel' && document.formvalidator.isValid(document.id('ringoocity-form'))) {
                
                Joomla.submitform(task, document.getElementById('ringoocity-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_ws_ringoo&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="ringoocity-form" class="form-validate">

	<div class="form-inline form-inline-header">
		<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('name'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('address'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('address'); ?></div>
		</div>
	</div>

    <div class="form-horizontal">
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_WS_RINGOO_LEGEND_RINGOOCITY', true)); ?>
        <div class="row-fluid">
            <div class="span9">
                <fieldset class="form-horizontal">

                <input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php if(empty($this->item->created_by)){ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo JFactory::getUser()->id; ?>" />

				<?php } 
				else{ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />

				<?php } ?>			
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('email'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('email'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('phone'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('phone'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('worktime'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('worktime'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('image'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('image'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label">Карта</div>
				<div class="controls">
					<input class="text_area" type="text" name="addresspicker_map" id="addresspicker_map" size="48" value="" />
					<div id="map"></div>
					<div id="legend">Можно передвигать метку для изменения локации</div>
				</div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('longitude'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('longitude'); ?>&nbsp;<?php echo $this->form->getInput('latitude'); ?></div>
			</div>



                </fieldset>
            </div>
			<div class="span3">
				<fieldset class="form-vertical">
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('region'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('region'); ?></div>
					</div>

					<?php
						foreach((array)$this->item->region as $value): 
							if(!is_array($value)):
								echo '<input type="hidden" class="region" name="jform[regionhidden]['.$value.']" value="'.$value.'" />';
							endif;
						endforeach;
					?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('redesign'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('redesign'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('redesignclosedate'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('redesignclosedate'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('redesignopendate'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('redesignopendate'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('redesignstatus'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('redesignstatus'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('language'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('language'); ?></div>
					</div>	
				</fieldset>			
			</div>




		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
        
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'otherparams', JText::_('COM_WS_RINGOO_GROUP_LABEL_SHOP_DETAILS', true)); ?>
		<div class="span9">
			<fieldset class="form-vertical">
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('info'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('info'); ?></div>
				</div>
			</fieldset>			
		</div>
		<div class="span3">
			<fieldset class="form-vertical offset1">
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('vlasniyrahunok'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('vlasniyrahunok'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('alfabank'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('alfabank'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('deltabank'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('deltabank'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('otpbank'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('otpbank'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('platinumbank'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('platinumbank'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('privatbank'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('privatbank'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('tickets'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('tickets'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('opendate'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('opendate'); ?></div>
				</div>
			</fieldset>
		</div>

	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php if (JFactory::getUser()->authorise('core.admin','ws_ringoo')) : ?>
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL', true)); ?>
			<?php echo $this->form->getInput('rules'); ?>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
	<?php endif; ?>

        <?php echo JHtml::_('bootstrap.endTabSet'); ?>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>