CREATE TABLE IF NOT EXISTS `#__ws_ringoo_region` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
`name` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__ws_ringoo_city` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
`name` VARCHAR(255)  NOT NULL ,
`region` INT NOT NULL ,
`address` VARCHAR(255)  NOT NULL ,
`email` VARCHAR(255)  NOT NULL ,
`phone` VARCHAR(10)  NOT NULL ,
`worktime` VARCHAR(255)  NOT NULL ,
`opendate` DATE NOT NULL ,
`redesignclosedate` DATE NOT NULL ,
`redesignopendate` DATE NOT NULL ,
`redesign` VARCHAR(255)  NOT NULL ,
`longitude` VARCHAR(255)  NOT NULL ,
`latitude` VARCHAR(255)  NOT NULL ,
`info` TEXT NOT NULL ,
`vlasniyrahunok` TINYINT(1)  NOT NULL ,
`tickets` TINYINT(1)  NOT NULL ,
`alfabank` TINYINT(1)  NOT NULL ,
`platinumbank` TINYINT(1)  NOT NULL ,
`otpbank` TINYINT(1)  NOT NULL ,
`privatbank` TINYINT(1)  NOT NULL ,
`deltabank` TINYINT(1)  NOT NULL ,
`image` VARCHAR(255)  NOT NULL ,
`language` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__ws_ringoo_superprice` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
`image` VARCHAR(255)  NOT NULL ,
`name` VARCHAR(255)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`oldprice` TINYINT(10)  NOT NULL ,
`newprice` TINYINT(10)  NOT NULL ,
`description` TEXT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;


CREATE TABLE IF NOT EXISTS `#__ws_ringoo_banner` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
`image` VARCHAR(255)  NOT NULL ,
`smallimage` VARCHAR(255)  NOT NULL ,
`name` VARCHAR(255)  NOT NULL ,
`linkmenu` INT(11)  NOT NULL ,
`company` VARCHAR(255)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;