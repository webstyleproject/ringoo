<?php
/**
 * @version     1.0.0
 * @package     com_ws_ringoo
 * @copyright   Copyright WebStyle project (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Svyat <admin@webstyle.pp.ua> - http://webstyle.pp.ua
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Ws_ringoo model.
 */
class Ws_ringooModelRingoocity extends JModelAdmin
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since	1.6
	 */
	protected $text_prefix = 'COM_WS_RINGOO';


	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Ringoocity', $prefix = 'Ws_ringooTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_ws_ringoo.ringoocity', 'ringoocity', array('control' => 'jform', 'load_data' => $loadData));
        
        
		if (empty($form)) {
			return false;
		}

		// Modify the form based on access controls
		if (!$this->canEditState((object) $data)){
			$form->setFieldAttribute('privatbank', 'disabled', 'true');
			$form->setFieldAttribute('privatbank', 'filter', 'unset');
		}
		// Modify the form based on access controls
		if (!$this->canEditState((object) $data)){
			$form->setFieldAttribute('platinumbank', 'disabled', 'true');
			$form->setFieldAttribute('platinumbank', 'filter', 'unset');
		}
		// Modify the form based on access controls
		if (!$this->canEditState((object) $data)){
			$form->setFieldAttribute('alfabank', 'disabled', 'true');
			$form->setFieldAttribute('alfabank', 'filter', 'unset');
		}
		// Modify the form based on access controls
		if (!$this->canEditState((object) $data)){
			$form->setFieldAttribute('deltabank', 'disabled', 'true');
			$form->setFieldAttribute('deltabank', 'filter', 'unset');
		}
		// Modify the form based on access controls
		if (!$this->canEditState((object) $data)){
			$form->setFieldAttribute('otpbank', 'disabled', 'true');
			$form->setFieldAttribute('otpbank', 'filter', 'unset');
		}
		// Modify the form based on access controls
		if (!$this->canEditState((object) $data)){
			$form->setFieldAttribute('wsredesign', 'disabled', 'true');
			$form->setFieldAttribute('wsredesign', 'filter', 'unset');
		}
		// Modify the form based on access controls
		if (!$this->canEditState((object) $data)){
			$form->setFieldAttribute('tickets', 'disabled', 'true');
			$form->setFieldAttribute('tickets', 'filter', 'unset');
		}
		// Modify the form based on access controls
		if (!$this->canEditState((object) $data)){
			$form->setFieldAttribute('vlasniyrahunok', 'disabled', 'true');
			$form->setFieldAttribute('vlasniyrahunok', 'filter', 'unset');
		}
		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_ws_ringoo.edit.ringoocity.data', array());

		if (empty($data)) {
			$data = $this->getItem();
            
		}

		return $data;
	}

	/**
	 * Method to bank and other records.
	 *
	 * @param   array    &$pks   The ids of the items to publish.
	 * @param   integer  $value  The value of the published state
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   1.6
	 */
	public function privatbank(&$pks, $value = 1){
		$user = JFactory::getUser();
		$table = $this->getTable();
		$pks = (array) $pks;

		// Access checks.
		foreach ($pks as $i => $pk){
			if ($table->load($pk)){
				if (!$this->canEditState($table)){
					// Prune items that you can't change.
					unset($pks[$i]);
					JError::raiseWarning(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
				}
			}
		}

		// Attempt to change the state of the records.
		if (!$table->privatbank($pks, $value, $user->get('id'))){
			$this->setError($table->getError());
			return false;
		}
		return true;
	}

	public function alfa(&$pks, $value = 1){
		$user = JFactory::getUser();
		$table = $this->getTable();
		$pks = (array) $pks;

		// Access checks.
		foreach ($pks as $i => $pk){
			if ($table->load($pk)){
				if (!$this->canEditState($table)){
					// Prune items that you can't change.
					unset($pks[$i]);
					JError::raiseWarning(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
				}
			}
		}

		// Attempt to change the state of the records.
		if (!$table->alfabank($pks, $value, $user->get('id'))){
			$this->setError($table->getError());
			return false;
		}
		return true;
	}

	public function delta(&$pks, $value = 1){
		$user = JFactory::getUser();
		$table = $this->getTable();
		$pks = (array) $pks;

		// Access checks.
		foreach ($pks as $i => $pk){
			if ($table->load($pk)){
				if (!$this->canEditState($table)){
					// Prune items that you can't change.
					unset($pks[$i]);
					JError::raiseWarning(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
				}
			}
		}

		// Attempt to change the state of the records.
		if (!$table->deltabank($pks, $value, $user->get('id'))){
			$this->setError($table->getError());
			return false;
		}
		return true;
	}

	public function platinum(&$pks, $value = 1){
		$user = JFactory::getUser();
		$table = $this->getTable();
		$pks = (array) $pks;

		// Access checks.
		foreach ($pks as $i => $pk){
			if ($table->load($pk)){
				if (!$this->canEditState($table)){
					// Prune items that you can't change.
					unset($pks[$i]);
					JError::raiseWarning(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
				}
			}
		}

		// Attempt to change the state of the records.
		if (!$table->platinumbank($pks, $value, $user->get('id'))){
			$this->setError($table->getError());
			return false;
		}
		return true;
	}

	public function otp(&$pks, $value = 1){
		$user = JFactory::getUser();
		$table = $this->getTable();
		$pks = (array) $pks;

		// Access checks.
		foreach ($pks as $i => $pk){
			if ($table->load($pk)){
				if (!$this->canEditState($table)){
					// Prune items that you can't change.
					unset($pks[$i]);
					JError::raiseWarning(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
				}
			}
		}

		// Attempt to change the state of the records.
		if (!$table->otpbank($pks, $value, $user->get('id'))){
			$this->setError($table->getError());
			return false;
		}
		return true;
	}

	public function wsredesign(&$pks, $value = 1){
		$user = JFactory::getUser();
		$table = $this->getTable();
		$pks = (array) $pks;

		// Access checks.
		foreach ($pks as $i => $pk){
			if ($table->load($pk)){
				if (!$this->canEditState($table)){
					// Prune items that you can't change.
					unset($pks[$i]);
					JError::raiseWarning(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
				}
			}
		}

		// Attempt to change the state of the records.
		if (!$table->redesign($pks, $value, $user->get('id'))){
			$this->setError($table->getError());
			return false;
		}
		return true;
	}

	public function tickets(&$pks, $value = 1){
		$user = JFactory::getUser();
		$table = $this->getTable();
		$pks = (array) $pks;

		// Access checks.
		foreach ($pks as $i => $pk){
			if ($table->load($pk)){
				if (!$this->canEditState($table)){
					// Prune items that you can't change.
					unset($pks[$i]);
					JError::raiseWarning(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
				}
			}
		}

		// Attempt to change the state of the records.
		if (!$table->tickets($pks, $value, $user->get('id'))){
			$this->setError($table->getError());
			return false;
		}
		return true;
	}

	public function vlasniyrahunok(&$pks, $value = 1){
		$user = JFactory::getUser();
		$table = $this->getTable();
		$pks = (array) $pks;

		// Access checks.
		foreach ($pks as $i => $pk){
			if ($table->load($pk)){
				if (!$this->canEditState($table)){
					// Prune items that you can't change.
					unset($pks[$i]);
					JError::raiseWarning(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
				}
			}
		}

		// Attempt to change the state of the records.
		if (!$table->vlasniyrahunok($pks, $value, $user->get('id'))){
			$this->setError($table->getError());
			return false;
		}
		return true;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk)) {

			//Do any procesing on fields here if needed

		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since	1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id)) {

			// Set ordering to the last item if not set
			if (@$table->ordering === '') {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__ws_ringoo_city');
				$max = $db->loadResult();
				$table->ordering = $max+1;
			}

		}
	}

}