<?php

/**
 * @version     1.0.0
 * @package     com_ws_ringoo
 * @copyright   Copyright WebStyle project (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Svyat <admin@webstyle.pp.ua> - http://webstyle.pp.ua
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Ws_ringoo records.
 */
class Ws_ringooModelRingoocitys extends JModelList {

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                                'id', 'a.id',
                'ordering', 'a.ordering',
                'state', 'a.state',
                'created_by', 'a.created_by',
                'name', 'a.name',
                'region', 'a.region',
                'address', 'a.address',
                'email', 'a.email',
                'phone', 'a.phone',
                'worktime', 'a.worktime',
                'opendate', 'a.opendate',
                'redesignclosedate', 'a.redesignclosedate',
                'redesignopendate', 'a.redesignopendate',
                'redesign', 'a.redesign',
                'longitude', 'a.longitude',
                'latitude', 'a.latitude',
                'info', 'a.info',
                'vlasniyrahunok', 'a.vlasniyrahunok',
                'tickets', 'a.tickets',
                'alfabank', 'a.alfabank',
                'platinumbank', 'a.platinumbank',
                'otpbank', 'a.otpbank',
                'state', 'privatbank', 'a.privatbank',
                'deltabank', 'a.deltabank',
                'image', 'a.image',
                'language', 'a.language',

            );
        }

        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     */
    protected function populateState($ordering = null, $direction = null) {
        // Initialise variables.
        $app = JFactory::getApplication('administrator');

        // Load the filter state.
        $search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        $published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
        $this->setState('filter.state', $published);

        
		//Filtering region
		$this->setState('filter.region', $app->getUserStateFromRequest($this->context.'.filter.region', 'filter_region', '', 'string'));

        //Filtering language
        $language = $this->getUserStateFromRequest($this->context . '.filter.language', 'filter_language', '');
        $this->setState('filter.language', $language);

        // force a language
        $forcedLanguage = $app->input->get('forcedLanguage');
        if (!empty($forcedLanguage))
        {
            $this->setState('filter.language', $forcedLanguage);
            $this->setState('filter.forcedLanguage', $forcedLanguage);
        }


        // Load the parameters.
        $params = JComponentHelper::getParams('com_ws_ringoo');
        $this->setState('params', $params);

        // List state information.
        parent::populateState('a.name', 'asc');
    }

    /**
     * Method to get a store id based on model configuration state.
     *
     * This is necessary because the model is used by the component and
     * different modules that might need different sets of data or different
     * ordering requirements.
     *
     * @param	string		$id	A prefix for the store id.
     * @return	string		A store id.
     * @since	1.6
     */
    protected function getStoreId($id = '') {
        // Compile the store id.
        $id.= ':' . $this->getState('filter.search');
        $id.= ':' . $this->getState('filter.state');
        $id.= ':' . $this->getState('filter.region');
        $id.= ':' . $this->getState('filter.language');

        return parent::getStoreId($id);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return	JDatabaseQuery
     * @since	1.6
     */
    protected function getListQuery() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query->select(
                $this->getState(
                        'list.select', 'DISTINCT a.*'
                )
        );
        $query->from('`#__ws_ringoo_city` AS a');

        
		// Join over the users for the checked out user
		$query->select("uc.name AS editor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the user field 'created_by'
		$query->select('created_by.name AS created_by');
		$query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');
		// Join over the foreign key 'region'
		$query->select('#__ws_ringoo_region.name AS ringooregions_name');
		$query->join('LEFT', '#__ws_ringoo_region AS #__ws_ringoo_region ON #__ws_ringoo_region.id = a.region');
        // Join over the language
        $query->select('l.title AS language_title')
            ->join('LEFT', $db->quoteName('#__languages') . ' AS l ON l.lang_code = a.language');
        

		// Filter by published state
		$published = $this->getState('filter.state');
		if (is_numeric($published)) {
			$query->where('a.state = ' . (int) $published);
		} else if ($published === '') {
			$query->where('(a.state IN (0, 1))');
		}

        // Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int) substr($search, 3));
            } else {
                $search = $db->Quote('%' . $db->escape($search, true) . '%');
                $query->where('( a.name LIKE '.$search.'  OR  a.address LIKE '.$search.' )');
            }
        }

        // Filter on the language.
        if ($language = $this->getState('filter.language'))
        {
            $query->where('a.language = ' . $db->quote($language));
        }        

		//Filtering region
		$filter_region = $this->state->get("filter.region");
		if ($filter_region) {
			$query->where("a.region = '".$db->escape($filter_region)."'");
		}


        // Add the list ordering clause.
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        if ($orderCol && $orderDirn) {
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        }

        return $query;
    }

    public function getItems() {
        $items = parent::getItems();
        
		foreach ($items as $oneItem) {

			if (isset($oneItem->region)) {
				$values = explode(',', $oneItem->region);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select($db->quoteName('name'))
							->from('`#__ws_ringoo_region`')
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->region = !empty($textValue) ? implode(', ', $textValue) : $oneItem->region;

			}
			//$oneItem->redesign = JText::_('COM_WS_RINGOO_RINGOOCITYS_REDESIGN_OPTION_' . strtoupper($oneItem->redesign));
		}
        return $items;
    }

}
