<?php
/**
 * @version	 1.0.0
 * @package	 com_ws_ringoo
 * @copyright   Copyright WebStyle project (C) 2014. All rights reserved.
 * @license	 GNU General Public License version 2 or later; see LICENSE.txt
 * @author	  Svyat <admin@webstyle.pp.ua> - http://webstyle.pp.ua
 */
// no direct access
defined('_JEXEC') or die;

// JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
// JHtml::_('bootstrap.tooltip');
// JHtml::_('behavior.multiselect');
// JHtml::_('formbehavior.chosen', 'select');

?>


<div class="ws-conv">
	<div id="wsmaps" class="visual-maps">
		<div id="map" class="a"></div>
	</div>

<?php
$metki = '';

	foreach ($this->items as $item){
				$metki .= 'var point = new ymaps.Placemark(';
				$metki .= '[';
				$metki .= $item->longitude;
				$metki .= ',' ;
				$metki .= $item->latitude; 
				$metki .= '], {';
				$metki .= 'balloonContentHeader: "';
				$metki .= $item->name; //город
				$metki .= '",';
				$metki .= 'balloonContentBody: "'; // тело балуна
				// if ($params->get('show_image')) {
				$metki .= $item->image; 
				// }
				$metki .= '<div class=\'wsdescl\'>';
				$metki .= $item->address; // адрес
				$metki .= '<br />';
				$metki .= $item->worktime; // график работы
				$metki .= '<br />';
				$metki .= '<b>';
				$metki .= 'тел: +38 ';
				$metki .= $item->phone; // телефон
				$metki .= '</b></div>'; 
				// if ($params->get('show_image') && $params->get('show_addimages')) { 
				// $metki .= $item->additionalimages; 
				// }
				$metki .= '<br class=\'clear\' />';
				$metki .= '",'; // тело балуна закончилось
				$metki .= 'balloonContentFooter: "';
				// if ($params->get('show_credit')) {
				$metki .= '<div class=\'ws-credit\'>';
				if ($item->alfabank == 1 ) {
				$metki .= '<a target=\'blank\' href=\'http://www.ringoo.ua/credit.html#alfa-bank\' title=\''. JText::_( 'WSMAPALFABANK' ) .'\' class=\'alfa opened\'></a>';
				}
				if ($item->otpbank == 1 ) {
				$metki .= '<a target=\'blank\' href=\'http://www.ringoo.ua/credit.html#otp-bank\' title=\''. JText::_( 'WSMAPOTPBANK' ) .'\' class=\'otp opened\'></a>';
				}
				if ($item->platinumbank == 1 ) {
				$metki .= '<a target=\'blank\' href=\'http://www.ringoo.ua/credit.html#platinum-bank\' title=\''. JText::_( 'WSMAPPLATINUMBANK' ) .'\' class=\'platinum opened\'></a>';
				}
				if ($item->deltabank == 1 ) {
				$metki .= '<a target=\'blank\' href=\'http://www.ringoo.ua/credit.html#delta-bank\' title=\''. JText::_( 'WSMAPDELTABANK' ) .'\' class=\'delta opened\'></a>';
				}
				if ($item->privatbank == 1 ) {
				$metki .= '<a target=\'blank\' href=\'http://www.ringoo.ua/credit.html#privat-bank\' title=\''. JText::_( 'WSMAPDELTABANK' ) .'\' class=\'privat opened\'></a>';
				}
				// if ($item->job == 1 ) {
				// $metki .= '<a target=\'blank\' href=\'http://www.ringoo.ua/jobs.html#tab-2\' title=\''. JText::_( 'WSMAPOPENVACANCY' ) .'\' class=\'vacancy\'>'. JText::_( 'WSMAPVACANCY' ) .'</a>';
				// }
				$metki .= '</div>';
				// }

				$metki .= '"},{';
				//$metki .= ' preset: \'twirl#greenIcon\''; 
				// $metki .= 'balloonContentSize: [600, 270], ';
				$metki .= 'balloonMinWidth: 400, ';
				$metki .= 'balloonMinHeight: 150, ';
				$metki .= 'iconImageHref: "/images/map/point.png", ';
				$metki .= 'iconImageSize: [37, 42], ';
				$metki .= 'iconImageOffset: [-11, -40] ';
				$metki .= '}';
				$metki .= ');'; 
				$metki .= 'myPoints.push(point);';
			}

$script ='
ymaps.ready(init);
function init() {
	var myMap = new ymaps.Map(\'map\', {
			center: [48.292586, 31.125952],
			zoom: 6,
			behaviors: [\'default\', \'dblClickZoom\']
		});


	var myPoints = [];
	
			'.$metki.'

	clusterer = new ymaps.Clusterer();
	clusterer.options.set({
		gridSize: 80,
		margin: 10,
		zoomMargin: 50,
		minClusterSize:4,
		clusterIcons: [{
			href: \'/images/map/cluster-small.png\',
			size: [47, 47],
			offset: [-23, -23]
		}, {
			href: \'/images/map/cluster-big.png\',
			size: [59, 59],
			offset: [-29, -29]
		}, {
			href: \'/images/map/cluster-bigger.png\',
			size: [72, 72],
			offset: [-36, -36]
		}]
	});
	clusterer.add(myPoints);

	myMap.geoObjects.add(clusterer);
	//Добавим копирайты
	myMap.copyrights.add(\'&copy; <a target="blank" href="http://webstyle.pp.ua">WebStyle project</a>\');

	// Добавляем контрол в верхний правый угол,
	myMap.controls
		.add(\'smallZoomControl\', { top: 70, left:8 })
		.add(\'typeSelector\', { top: 30, left: 100 }) // Список типов карты
		.add(\'mapTools\', { top: 30 }) // Стандартный набор кнопок
};
';

echo '<script type="text/javascript">jQuery(document).ready(function($){';
echo $script;
echo '});</script>';
// echo '<script type="text/javascript" src="http://api-maps.yandex.ru/2.0.29/?load=package.standard,package.clusters&lang='. JText::_( 'WSMAPLANG' ) .'&onload=init"></script>';
echo '<script type="text/javascript" src="http://api-maps.yandex.ru/2.0.29/?load=package.standard,package.clusters&lang=uk-UA&onload=init"></script>';
echo '<script type="text/javascript">
jQuery(document).ready(function($){
	// Initialise Plugin
	var options1 = {
		additionalFilterTriggers: [$(\'#quickfind\')],
		clearFiltersControls: [$(\'#linkmap\')],
		matchingRow: function(state, tr, textTokens) {		  
		if (!state || !state.id) { return true; }
	var child = tr.children(\'td:eq(2)\');
		if (!child) return true;
	var val =  child.text();		  
		switch (state.id) {
		case "onlyyes": return state.value !== true || val === "yes";
		case "onlyno": return state.value !== true || val === "no";
		default: return true;
		}}};
	$(\'#shops\').tableFilter(options1);
});
</script>';
echo '<script language="javascript" type="text/javascript" src="media/picnet/picnet.js"></script>';
echo '<script language="javascript" type="text/javascript" src="media/ws/toggle.js"></script>';
?>

<div id="wslists" class="visual-list">

<?php echo JText::_( 'WSSEARCHSHOP' ) ?>: <input id="quickfind" type="text" /> <a id="linkmap" class="linkmap" href="#"><?php echo JText::_( 'WSCLEANSEARCH' ) ?></a>

<div class="clear"></div>
<table id="shops">
<thead> 
	<tr>
		<th class="psc"><?php echo JText::_( 'WSSEARCHCITY' ) ?></th>
		<th class="adress"><?php echo JText::_( 'WSSEARCHADRESS' ) ?></th>
		<th class="psc"><?php echo JText::_( 'WSSEARCHPHONES' ) ?></th>
		<th class="psc"><?php echo JText::_( 'WSSEARCHTIMEWORK' ) ?></th>
	</tr>
</thead> 
<tbody>

<?php foreach ($this->items as $item) : ?>

<tr>
	<td>
		<?php echo $item->name; ?>
	</td>
	<td>
		<?php echo $item->address; ?>
	</td>
	<td>
		<?php echo JText::_( 'WSPHONE' ) ?><?php echo $item->phone; ?>
	</td>
	<td>
		<?php echo $item->worktime; ?>
	</td>
</tr>
<?php endforeach; ?>
</tbody>
</table>

</div>

<div class="ws-selector">
	<span class="left"><?php echo JText::_( 'WSSHOPMAP' ) ?></span>
	<i class="slider-ico">
		<i></i>
	</i>
	<span class="right"><?php echo JText::_( 'WSSHOPLIST' ) ?></span>
</div>

</div>
