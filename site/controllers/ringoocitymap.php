<?php
/**
 * @version     1.0.0
 * @package     com_ws_ringoo
 * @copyright   Copyright WebStyle project (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Svyat <admin@webstyle.pp.ua> - http://webstyle.pp.ua
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Ringoocitys list controller class.
 */
class Ws_ringooControllerRingoocitymap extends Ws_ringooController
{
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function &getModel($name = 'Ringoocitymap', $prefix = 'Ws_ringooModel')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
}