<?php

/**
 * @version     1.0.0
 * @package     com_ws_ringoo
 * @copyright   Copyright WebStyle project (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Svyat <admin@webstyle.pp.ua> - http://webstyle.pp.ua
 */
defined('_JEXEC') or die;

class Ws_ringooFrontendHelper {
    
	/**
	* Get menuitem name using group ID
	* @param integer $menuitem_id Menuitem ID
	* @return mixed menuitem name if the menuitem was found, null otherwise
	*/
	public static function getMenuItemNameByMenuItemId($menuitem_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select('title')
			->from('#__menu')
			->where('id = ' . intval($menuitem_id));

		$db->setQuery($query);
		return $db->loadResult();
	}
}
